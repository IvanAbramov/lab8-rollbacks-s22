import psycopg2

conn = psycopg2.connect(
    "dbname=lab host=localhost password=postgres user=postgres port=5431"
)

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"


increase_product_inventory = """
INSERT INTO Inventory (username, product, amount) 
VALUES (%(username)s, %(product)s, %(amount)s)
ON CONFLICT (username, product) DO UPDATE 
SET amount = Inventory.amount + %(amount)s
WHERE (Inventory.product = %(product)s AND Inventory.username = %(username)s)"""

check_inventory_limit = (
    "SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"
)


def buy_product(username, product, amount):
    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            try:
                cur.execute(check_inventory_limit, obj)
                products_amount = cur.fetchone()[0]
                if products_amount is None:
                    products_amount = 0
                if products_amount + amount > 100:
                    raise Exception("100 products limit exceeded")
                cur.execute(increase_product_inventory, obj)
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Wrong username, product or amount")

            conn.commit()

buy_product("Bob", "marshmello", 10)
